#include<iostream>
#include<ctime>				//for use time 
#include<cstdlib>			//for random function
using namespace std;		

const int x = 10;
const int y = 10;
int table(int map[][10]);
void PlayerMove(int map[][10]);
void numRider(int level, int map[][10]);
int riderMove(int map[][10]);
int useRing(int ring, int map[][10]);


int main(){
	int map[x][y]={};	
	char name[10];		
	int level=1;		
	int lose=0;			
	int ring;			
	int c=0;			
	cout << "Frodo & The Lord of the Rings Game!\n\n"
			<<"Frodo(F) is trapped in the Dark Forest take him to the Exit(E)\nBeware the Dark Rider(R) it'll see you when you approach it\n"		
			<<"by the way Frodo has the ring, which he can use once per level, \nto become invisible and quickly move 3 times while the Riders are frozen.\n\n";
			
		cout <<   "   **RULE**\n    Control\n"
				<<"  ============\n"
				<<"    \\  |  /   \n"
				<<"     7 8 9   \n"
				<<"  <- 4   6 ->\n"
				<<"     1 2 3   \n"
				<<"    /  |  \\   " << endl
				<<"  ============ \n";
	cout << "Enter your name : ";
	cin >> name;	
	cout << "Start\n";
	

	while(level <= 10){								//loop will replay until stage 10
	cout << ">>> Stage " << level <<" <<<\n";
		ring=1;										//give player 1 ring per stage
		map[9][9]=3;								//in array[9][9] is exit
		map[0][0]=1;								//in array[1][1] is Frodo start for every stage
		numRider(level, map);							//call function for number of Dark Rider
		table(map);									//call function for show table grid
		
		while(lose!=1){								//if lose=1 stop loop
			
			if(ring != 0){							
				ring = useRing(ring, map);
					if(ring==0){					//if player select "yes" get in this part
						while(c<3){					//player move 3 times
						PlayerMove(map);				
						table(map);			
						c++;						//count for move
						}							
					lose = riderMove(map);				
					}
				}									
				
			PlayerMove(map);
    		lose = riderMove(map);
    		table(map);
			if(map[9][9]==4){						//if player move Frodo to exit, stop loop
			break;
			}
			
		}
		if(lose==1){								
			break;			
		}else{
			table(map);
			cout << "Next stage\n";
			level++;								//count level
		}
	
	}
	if(lose==1){
		cout << "You lose\n";
	}else
	cout << "\n\n\n	  Win!!!\nThank you for helping Frodo";
	
	return 0;
}


int table(int map[x][y]){										//function show table 10x10
	for(int i=0; i<10; i++){							
			cout << "---------------------\n|";
			for(int j=0; j<10; j++){				
				if(map[i][j]==1){		 
					cout << "F|";
				}else if(map[i][j]==2){
					cout << "R|";		 
				}else if(map[i][j]==3){
					cout << "E|";	
				}else if(map[i][j]==4){
					cout << "N|";		
				}else if(map[i][j]==5){
					cout << "L|";
				}else
				cout << " |";
			}
			cout << endl;
		}
		cout << "---------------------\n";
}

void numRider(int level, int map[x][y]){							//function set number of Dark Rider per level (add 3 Dark Riders per level)
	int i, j, count=1;
	srand(time(0));	

	for(i=0; i<10; i++){
		for(j=0; j<10; j++){
			if(map[i][j]==2){
				map[i][j]=0;						//reset number and position of Dark Rider
			}
		}
	}
	
	while(count <= level*3){						
		i = rand()%10;
		j = rand()%10;
		while(i<3 || (i==9 && j==9)){
		i = rand()%10;
		j = rand()%10;
		}
		map[i][j]  = 2;
		count++;
	}
}

int riderMove(int map[x][y]){									//function random direction for Dark Rider
	int walk;
	srand(time(0));
	
	for(int i=0; i<10; i++){
		for(int j=0; j<10; j++){
			if(map[i][j]==2){
				if(map[i][j]==2 && (map[i][j+1]==1 || map[i][j-1]==1 || map[i-1][j]==1 || map[i+1][j]==1 || map[i-1][j+1]==1 || map[i-1][j-1]==1 || map[i+1][j+1]==1 || map[i+1][j-1]==1)){
					map[i][j] = 5;
					return 1;
				}
			walk = rand()%10;
		while(walk==5 || walk==0){	
			walk = rand()%10;
		}
			if(walk==6 ){	//move right			
				if(map[i][j]==2 && map[i][j+1]==0){
				map[i][j+1] = 2;
				map[i][j] = 0;
				break;	
				}
			}else
			if(walk==4 ){	//move left
				if((map[i][j]==2 && map[i][j-1]==0)){
				map[i][j-1] = 2;
				map[i][j]=0;
				break;
				}
			}else
			if(walk==8 ){	//move up
				if(map[i][j]==2 && map[i-1][j]==0){
				map[i-1][j] = 2;
				map[i][j]=0;
				break;
				}
			}else
			if(walk==2){	//move down
				if(map[i][j]==2 && map[i+1][j]==0){
				map[i+1][j] = 2;
				map[i][j]=0;
				break;
				}
			}else 
			if(walk==9  ){	//move diagonal up-right(/)
				if(map[i][j]==2 && map[i-1][j+1]==0){
				map[i-1][j+1] = 2;
				map[i][j]=0;
				break;
				}
			}else
			if(walk==7){	//move diagonal up-left(\)
				if(map[i][j]==2 && map[i-1][j-1]==0){
				map[i-1][j-1] = 2;
				map[i][j]=0;
				break;
				}
			}else
			if(walk==3 ){	//move diagonal down-right(\)
				if(map[i][j]==2 && map[i+1][j+1]==0){
				map[i+1][j+1] = 2;
				map[i][j]=0;				
				break;
				}
			}else
			if(walk==1 ){	//move diagonal down-left(/)
				if(map[i][j]==2 && map[i+1][j-1]==0){
				map[i+1][j-1] = 2;
				map[i][j]=0;
				break;
				}
			}


	
			}
		}
	}
	return 0;
}

int useRing(int ring, int map[x][y]){												//function ask are player want to use ring
	int use;
	if(ring == 1){
	cout << "Use ring ?\n[1] = Yes	[2] = No	...";
	cin >> use;
		
		switch(use){
			case 1:
				cout << "Move 3 times while Dark Rider frozen\n";
				return 0;
			case 2:
				cout << endl;
				return 1;
			default :
				cout << "Try again \n[1] = Yes	[2] = No	...";
				cin >> use;	
		}
	}
}

void PlayerMove(int map[x][y]){													//function set dierction that player can move Frodo
	int  walk;
	
	while((walk==5 || walk<=0 || walk >9) ){ 		//while no direction to move user can move again

	cout << "Enter direction...";
	cin >> walk;	
	
	if(walk==6){	//move right
		for(int i=0; i<10; i++){
			for(int j=0; j<9; j++){
				if(map[i][j]==1 && map[i][j+1]==0){				
					map[i][j+1] = 1;
					map[i][j]=0;
					break;
				}else 
				if(map[i][j]==1 && map[i][j+1]==3){
					map[i][j+1]=4;
					map[i][j]=0;
					
				}				
			}	
		}	
	}else 
	if(walk==4){	//move left
		for(int i=0; i<10; i++){
			for(int j=1; j<10; j++){				
				if(map[i][j]==1 && map[i][j-1]==0){
				map[i][j-1] = 1;
				map[i][j]=0;
				break;
				}
			}		
		}	
	}else
	if(walk==8){	//move up
		for(int j=0; j<10; j++){
			for(int i=1; i<10; i++){
				if(map[i][j]==1 && map[i-1][j]==0){
				map[i-1][j] = 1;
				map[i][j]=0;
				break;
				}
			}		
		}	
	}else
	if(walk==2){	//move down
		for(int j=0; j<10; j++){
			for(int i=0; i<9; i++){
				if(map[i][j]==1 && map[i+1][j]==0){
				map[i+1][j] = 1;
				map[i][j]=0;
				break;
				}else 
				if(map[i][j]==1 && map[i+1][j]==3){
					map[i+1][j]=4;
					map[i][j]=0;
					
				}				
			}		
		}	
	}else
	if(walk==9){	//move diagonal up-right(/)
		for(int i=1; i<10; i++){
			for(int j=0; j<9; j++){
				if(map[i][j]==1 && map[i-1][j+1]==0){
				map[i-1][j+1] = 1;
				map[i][j]=0;
				break;
				}
			}		
		}	
	}else
	if(walk==7){	//move diagonal up-left(\)
		for(int i=1; i<10; i++){
			for(int j=1; j<10; j++){
				if(map[i][j]==1 && map[i-1][j-1]==0){
				map[i-1][j-1] = 1;
				map[i][j]=0;
				break;
				}
			}		
		}	
	}else
	if(walk==3){	//move diagonal down-right(\)
		for(int i=8; i>=0; i--){
			for(int j=0; j<9; j++){
				if(map[i][j]==1 && map[i+1][j+1]==0){
				map[i+1][j+1] = 1;
				map[i][j]=0;				
				break;
				}else 
				if(map[i][j]==1 && map[i+1][j+1]==3){
					map[i+1][j+1]=4;
					map[i][j]=0;
					
				}				
			}		
		}	
	}else
	if(walk==1){	//move diagonal down-left(/)
		for(int i=8; i>=0; i--){
			for(int j=1; j<10; j++){
				if(map[i][j]==1 && map[i+1][j-1]==0){
				map[i+1][j-1] = 1;
				map[i][j]=0;
				break;
				}
			}		
		}	
	}else
		cout << "Try Again \n";
	}
}


